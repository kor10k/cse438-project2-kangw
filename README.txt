Any information you think we should know about your submission
* Is there anything that doesn't work? Why?
* Is there anything that you did that you feel might be unclear? Explain it here.

Everything works fine as intended.

A description of the creative portion of the assignment
* Describe your feature
* Why did you choose this feature?
* How did you implement it?

I have two different parts for the creative portion

1. Lyrics button
    - In the songdetailactivity which is prompted when the user presses a track from the list, there is a lyrics button
      that uses webView to pull up a google search of the song's lyric

2. Suggestion of similar tracks
    - At the bottom of the songdetailactivity, there is a gridview of tracks similar to the track user chose to look into.
      This uses a .getSimilar method in the API to get data.
      
(10 / 10 points) The app displays the current top tracks in a GridView on startup
(10 / 10 points) The app uses a tab bar with two tabs, one for searching for tracks and one for looking at the playlist
(10 / 10 points) Data is pulled from the API and processed into a GridView on the main page. Makes use of a Fragment to display the results seamlessly.
(15 / 15 points) Selecting a track from the GridView opens a new activity with the track cover, title, and 3 other pieces of information as well as the ability to save it to the playlist.
(5 / 5 points) User can change search query by editing text field.
(10 / 10 points) User can save a track to their playlist, and the track is saved into a SQLite database.
(5 / 5 points) User can delete a track from the playlist (deleting it from the SQLite database itself).
(4 / 4 points) App is visually appealing
(1/ 1 point) Properly attribute Last.fm API as source of data.
(5 / 5 points) Code is well formatted and commented.
(10 / 10 points) All API calls are done asynchronously and do not stall the application.
(8 / 15 points) Creative portion: Be creative!
  The lyrics button is a nice addition, but trivial. The suggested tracks is much better, and I would've given full credit had you gone even further with it...like maybe allowing users to click those tracks and interact with them?
  
Total: 93 / 100

Total: 100 / 100
