package com.example.playlistapp

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient

class googleResult : AppCompatActivity() {
    // using webView displays the url for the lyric
    private var webView : WebView? = null
    private lateinit var url: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_google_result)

        webView = findViewById<WebView>(R.id.webView)
        url = intent.extras!!.getSerializable("URL") as String

        webView!!.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                view?.loadUrl(url)
                return true
            }
        }
        webView!!.loadUrl(url)
    }
}
