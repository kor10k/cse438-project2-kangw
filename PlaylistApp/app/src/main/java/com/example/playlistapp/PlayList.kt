package com.example.playlistapp

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentPagerAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_play_list.*
import kotlinx.android.synthetic.main.playlist_item.view.*

@SuppressLint("ValidFragment")
// fragment for the playlist tab
// gets data from the sql database and displays the playlist
class PlayList: Fragment() {
    private var adapter = PlayListAdapter()
    private lateinit var viewModel: PlayViewModel

    private var PlayList: ArrayList<plSet> = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_play_list, container, false)
    }

    override fun onStart() {
        super.onStart()

        playList_list.layoutManager = GridLayoutManager(this.context, 1)
        playList_list.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        viewModel = ViewModelProviders.of(this).get(PlayViewModel::class.java)

        val observer = Observer<ArrayList<plSet>> {
            playList_list.adapter = adapter
            val result = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
                override fun areItemsTheSame(p0: Int, p1: Int): Boolean {
                    if(p0 >= PlayList.size || p1 >= PlayList.size) {
                        return false
                    }
                    return PlayList[p0].name == PlayList[p1].name
                }

                override fun getOldListSize(): Int {
                    return PlayList.size
                }

                override fun getNewListSize(): Int {
                    if (it == null) {
                        return 0
                    }
                    return it.size
                }

                override fun areContentsTheSame(p0: Int, p1: Int): Boolean {
                    return PlayList[p0] == PlayList[p1]
                }
            })
            result.dispatchUpdatesTo(adapter)
            PlayList = it ?: ArrayList()
        }

        viewModel.getPlayList().observe(this, observer)
    }

    inner class PlayListAdapter: RecyclerView.Adapter<PlayListAdapter.PlayListViewHolder>() {

        override fun onCreateViewHolder(p0: ViewGroup, p1: Int): PlayListViewHolder {
            val itemView = LayoutInflater.from(p0.context).inflate(R.layout.playlist_item, p0, false)
            return PlayListViewHolder(itemView)
        }

        override fun onBindViewHolder(p0: PlayListAdapter.PlayListViewHolder, p1: Int) {
            val song = PlayList[p1]
            p0.name.text = song.name
            p0.artist.text = song.artist

            // when the user presses the delete button, removes the song from the database
            p0.deleteButton.setOnClickListener {
                viewModel.delete(this@PlayList.context!!, song.name)

                val text = "Removed from Your Playlist"
                val duration = Toast.LENGTH_SHORT


                val toast = Toast.makeText(this@PlayList.context, text, duration)
                toast.show()



            }
        }
        override fun getItemCount(): Int {
            return PlayList.size
        }

        inner class PlayListViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

            val deleteButton: Button = itemView.remove_bt
            var name: TextView = itemView.songName
            var artist: TextView = itemView.artistName
        }


    }
}
