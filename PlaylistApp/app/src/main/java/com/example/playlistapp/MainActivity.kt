package com.example.playlistapp

import android.content.Context
import android.net.ConnectivityManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    // initially displays the home tab
    var context: Context = this


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        val tabAdapter = MyPagerAdapter(this, supportFragmentManager)
        viewPager_main.adapter = tabAdapter

        tabs_main.setupWithViewPager(viewPager_main)

    }

    override fun onBackPressed() {
        super.onBackPressed()
        this.finish()
    }

   // adapter for the tabs to switch between  home tab and the playlist tab
    class MyPagerAdapter(context: Context, fm: FragmentManager) : FragmentPagerAdapter(fm) {
        var context = context

        override fun getItem(position: Int): Fragment {
            return when (position) {
                0 -> {
                    SongList(this.context)
                }
                else -> PlayList()
            }
        }

        override fun getCount(): Int {
            return 2
        }

        override fun getPageTitle(position: Int): CharSequence {
            return when (position) {
                0 -> "HOME"
                else -> {
                    return "PLAYLIST"
                }
            }
        }
    }
}
