package com.example.playlistapp

import java.io.Serializable
class Song (): Serializable {
    // class to store the song info
    private var songName: String = ""
    var duration: String = ""
    private var playcount: String = ""
    private var listeners: String = ""
    var songUrl: String = ""
    lateinit var artist: Artist
    var image: ArrayList<String> = ArrayList()

    constructor(
        songName: String,
        duration: String,
        playcount: String,
        listeners: String,
        songUrl: String,
        artist: Artist,
        image: ArrayList<String>
    ) : this() {
        this.songName = songName
        this.duration = duration
        this.playcount = playcount
        this.listeners = listeners
        this.songUrl = songUrl
        this.artist = artist
        this.image = image
    }

    fun getSongName(): String {
        return this.songName
    }

    fun getPlayCount(): String {
        return this.playcount
    }

    fun getListener(): String {
        return this.listeners
    }
}