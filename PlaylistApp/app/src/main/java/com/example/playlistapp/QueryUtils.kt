package com.example.playlistapp


import android.text.TextUtils
import android.util.Log
//import com.example.cse438.studio3.model.Offer
import com.example.playlistapp.Song
//import com.example.cse438.studio3.model.SiteDetail
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import java.nio.charset.Charset
import kotlin.collections.ArrayList

class cQueryUtils {
    // query setting for getting APIs

    companion object {
        private val LogTag = this::class.java.simpleName
        private const val BaseURL = "http://ws.audioscrobbler.com/2.0/"


        fun fetchSongData(jsonQueryString: String): ArrayList<Song>? {
            val url: URL? = createUrl("${this.BaseURL}$jsonQueryString")

            var jsonResponse: String? = null
            try {
                jsonResponse = makeHttpRequest(url)
            } catch (e: IOException) {
                Log.e(this.LogTag, "Problem making the HTTP request.", e)
            }

            return extractSongDataFromJson(jsonResponse)
        }

        fun fetchArtistSongData(jsonQueryString: String): ArrayList<Song>? {
            val url: URL? = createUrl("${this.BaseURL}$jsonQueryString")

            var jsonResponse: String? = null
            try {
                jsonResponse = makeHttpRequest(url)
            } catch (e: IOException) {
                Log.e(this.LogTag, "Problem making the HTTP request.", e)
            }

            return extractArtistSongDataFromJson(jsonResponse)
        }

        fun fetchSimilarSongData(jsonQueryString: String): ArrayList<Song>? {
            val url: URL? = createUrl("${this.BaseURL}$jsonQueryString")

            var jsonResponse: String? = null
            try {
                jsonResponse = makeHttpRequest(url)
            } catch (e: IOException) {
                Log.e(this.LogTag, "Problem making the HTTP request.", e)
            }

            return extractSimilarSongDataFromJson(jsonResponse)
        }

        private fun createUrl(stringUrl: String): URL? {
            var url: URL? = null
            try {
                url = URL(stringUrl)
            } catch (e: MalformedURLException) {
                Log.e(this.LogTag, "Problem building the URL.", e)
            }

            return url
        }

        private fun makeHttpRequest(url: URL?): String {
            var jsonResponse = ""

            if (url == null) {
                return jsonResponse
            }

            var urlConnection: HttpURLConnection? = null
            var inputStream: InputStream? = null
            try {
                urlConnection = url.openConnection() as HttpURLConnection
                urlConnection.readTimeout = 10000 // 10 seconds
                urlConnection.connectTimeout = 15000 // 15 seconds
                urlConnection.requestMethod = "GET"
                urlConnection.connect()

                if (urlConnection.responseCode == 200) {
                    inputStream = urlConnection.inputStream
                    jsonResponse = readFromStream(inputStream)
                } else {
                    Log.e(this.LogTag, "Error response code: ${urlConnection.responseCode}")
                }
            } catch (e: IOException) {
                Log.e(this.LogTag, "Problem retrieving the song data results: $url", e)
            } finally {
                urlConnection?.disconnect()
                inputStream?.close()
            }

            return jsonResponse
        }

        private fun readFromStream(inputStream: InputStream?): String {
            val output = StringBuilder()
            if (inputStream != null) {
                val inputStreamReader = InputStreamReader(inputStream, Charset.forName("UTF-8"))
                val reader = BufferedReader(inputStreamReader)
                var line = reader.readLine()
                while (line != null) {
                    output.append(line)
                    line = reader.readLine()
                }
            }

            return output.toString()
        }


        //for extracting json results from getSimilar function in the API
        private fun extractSimilarSongDataFromJson(songJson: String?): ArrayList<Song>? {
            if (TextUtils.isEmpty(songJson)) {
                return null
            }

            val songList = ArrayList<Song>()
            try {
                val baseJsonResponse = JSONObject(songJson)



                val tracks = returnValueOrDefault<JSONObject>(baseJsonResponse, "similartracks") as? JSONObject

                if (tracks != null) {

                    val songs = returnValueOrDefault<JSONArray>(tracks, "track") as JSONArray?
                    if (songs != null) {
                        for (k in 0 until songs.length()) {
                            val song = songs.getJSONObject(k)
                            val artists = returnValueOrDefault<JSONObject>(song, "artist") as JSONObject

                            //artist
                            val artistObjects = Artist(
                                returnValueOrDefault<String>(artists, "name") as String,
                                returnValueOrDefault<String>(artists, "Mbid") as String,
                                returnValueOrDefault<String>(artists, "url") as String
                            )

                            //image
                            val images = returnValueOrDefault<JSONArray>(song, "image") as JSONArray?
                            val imageArrayList = ArrayList<String>()
                            if (images != null) {
                                for (x in 0 until images.length()) {
                                    val image = images.getJSONObject(x)
                                    val imageUrl = returnValueOrDefault<String>(image, "#text") as String
                                    imageArrayList.add(imageUrl)
                                }
                            }

                            //songName, duration, playcount, listners, Mbid, songUrl
                            songList.add(
                                Song(
                                    returnValueOrDefault<String>(song, "name") as String,
                                    returnValueOrDefault<String>(song, "duration") as String,
                                    returnValueOrDefault<String>(song, "playcount") as String,
                                    returnValueOrDefault<String>(song, "listeners") as String,
                                    returnValueOrDefault<String>(song, "url") as String,
                                    artistObjects,
                                    imageArrayList
                                )
                            )
                        }
                    }

                }
            } catch (e: JSONException) {
                Log.e(this.LogTag, "Problem parsing the product JSON results", e)
            }

            return songList

        }

        // for extracting json results from artistTopTrack
        private fun extractArtistSongDataFromJson(songJson: String?): ArrayList<Song>? {
            if (TextUtils.isEmpty(songJson)) {
                return null
            }

            val songList = ArrayList<Song>()
            try {
                val baseJsonResponse = JSONObject(songJson)



                val tracks = returnValueOrDefault<JSONObject>(baseJsonResponse, "toptracks") as? JSONObject

                if (tracks != null) {

                    val songs = returnValueOrDefault<JSONArray>(tracks, "track") as JSONArray?
                    if (songs != null) {
                        for (k in 0 until songs.length()) {
                            val song = songs.getJSONObject(k)
                            val artists = returnValueOrDefault<JSONObject>(song, "artist") as JSONObject

                            //artist
                            val artistObjects = Artist(
                                returnValueOrDefault<String>(artists, "name") as String,
                                returnValueOrDefault<String>(artists, "Mbid") as String,
                                returnValueOrDefault<String>(artists, "url") as String
                            )

                            //image
                            val images = returnValueOrDefault<JSONArray>(song, "image") as JSONArray?
                            val imageArrayList = ArrayList<String>()
                            if (images != null) {
                                for (x in 0 until images.length()) {
                                    val image = images.getJSONObject(x)
                                    val imageUrl = returnValueOrDefault<String>(image, "#text") as String
                                    imageArrayList.add(imageUrl)
                                }
                            }

                            //songName, duration, playcount, listners, Mbid, songUrl
                            songList.add(
                                Song(
                                    returnValueOrDefault<String>(song, "name") as String,
                                    returnValueOrDefault<String>(song, "duration") as String,
                                    returnValueOrDefault<String>(song, "playcount") as String,
                                    returnValueOrDefault<String>(song, "listeners") as String,
                                    returnValueOrDefault<String>(song, "url") as String,
                                    artistObjects,
                                    imageArrayList
                                )
                            )
                        }
                    }

                }
            } catch (e: JSONException) {
                Log.e(this.LogTag, "Problem parsing the product JSON results", e)
            }

            return songList

        }

        // for extracting json result from the toptrack
        private fun extractSongDataFromJson(songJson: String?): ArrayList<Song>? {
            if (TextUtils.isEmpty(songJson)) {
                return null
            }

            val songList = ArrayList<Song>()
            try {
                val baseJsonResponse = JSONObject(songJson)



                val tracks = returnValueOrDefault<JSONObject>(baseJsonResponse, "tracks") as? JSONObject

                if (tracks != null) {

                    val songs = returnValueOrDefault<JSONArray>(tracks, "track") as JSONArray?
                    if (songs != null) {
                        for (k in 0 until songs.length()) {
                            val song = songs.getJSONObject(k)
                            val artists = returnValueOrDefault<JSONObject>(song, "artist") as JSONObject

                            //artist
                            val artistObjects = Artist(
                                returnValueOrDefault<String>(artists, "name") as String,
                                returnValueOrDefault<String>(artists, "Mbid") as String,
                                returnValueOrDefault<String>(artists, "url") as String
                            )

                            //image
                            val images = returnValueOrDefault<JSONArray>(song, "image") as JSONArray?
                            val imageArrayList = ArrayList<String>()
                            if (images != null) {
                                for (x in 0 until images.length()) {
                                    val image = images.getJSONObject(x)
                                    val imageUrl = returnValueOrDefault<String>(image, "#text") as String
                                    imageArrayList.add(imageUrl)
                                }
                            }

                            //songName, duration, playcount, listners, Mbid, songUrl
                            songList.add(
                                Song(
                                    returnValueOrDefault<String>(song, "name") as String,
                                    returnValueOrDefault<String>(song, "duration") as String,
                                    returnValueOrDefault<String>(song, "playcount") as String,
                                    returnValueOrDefault<String>(song, "listeners") as String,
                                    returnValueOrDefault<String>(song, "url") as String,
                                    artistObjects,
                                    imageArrayList
                                )
                            )
                        }
                    }

                }
            } catch (e: JSONException) {
                Log.e(this.LogTag, "Problem parsing the product JSON results", e)
            }

            return songList

        }


        private inline fun <reified T> returnValueOrDefault(json: JSONObject, key: String): Any? {
            when (T::class) {
                String::class -> {
                    return if (json.has(key)) {
                        json.getString(key)
                    } else {
                        ""
                    }
                }
                Int::class -> {
                    return if (json.has(key)) {
                        json.getInt(key)
                    } else {
                        return -1
                    }
                }
                Double::class -> {
                    return if (json.has(key)) {
                        json.getDouble(key)
                    } else {
                        return -1.0
                    }
                }
                Long::class -> {
                    return if (json.has(key)) {
                        json.getLong(key)
                    } else {
                        return (-1).toLong()
                    }
                }
                JSONObject::class -> {
                    return if (json.has(key)) {
                        json.getJSONObject(key)
                    } else {
                        return null
                    }
                }
                JSONArray::class -> {
                    return if (json.has(key)) {
                        json.getJSONArray(key)
                    } else {
                        return null
                    }
                }
                else -> {
                    return null
                }
            }
        }
    }
}