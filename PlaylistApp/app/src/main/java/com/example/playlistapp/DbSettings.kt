package com.example.playlistapp

import android.provider.BaseColumns

class DbSettings {
    // database for the playlist
    companion object {
        const val DB_NAME = "playlists.db"
        const val DB_VERSION = 1
    }

    class DBPlayListEntry: BaseColumns {
        companion object {
            const val TABLE = "playlist"
            const val ID = BaseColumns._ID
            const val COL_SONG = "song"
            const val COL_ARTIST = "artist"
        }
    }
}