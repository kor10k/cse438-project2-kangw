package com.example.playlistapp

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.util.DiffUtil
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_song_detail.*
import kotlinx.android.synthetic.main.activity_song_detail.view.*

class SongDetailActivity : AppCompatActivity() {
    // when the user press on a track, this activity is prompted
    private lateinit var song: Song
    private lateinit var artist: String
    private lateinit var count: String
    private lateinit var listener: String

    private var adapter = SimilarAdapter()
    private var SongList: ArrayList<Song> = ArrayList()
    private lateinit var viewModel: SongViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_song_detail)

        song = intent.extras!!.getSerializable("SONG") as Song
        artist = intent.extras!!.getSerializable("ARTIST") as String
        count = intent.extras!!.getSerializable("COUNT") as String
        listener = intent.extras!!.getSerializable("LISTENER") as String


        songName.text = "Song: " + song.getSongName()
        artistName.text = "Artist: " + artist
        playCount.text = "Playcount: " + song.getPlayCount()
        songListners.text = "Listeners: " + song.getListener()

        Picasso.with(this@SongDetailActivity).load(song.image[1]).into(songImage)

        // adds the current song user is viewing to the sql database for the playlist
        addButton.setOnClickListener {

            val db = PlayListDatabaseHelper(this)
            db.addSong(song.getSongName(), artist)

            val text = "Added to Your Playlist ${song.getSongName()}"
            val duration = Toast.LENGTH_SHORT

            val toast = Toast.makeText(this, text, duration)
            toast.show()

        }

        // prompts the webView activity to show the lyric of the song
        googleButton.setOnClickListener {
            val intent = Intent(this@SongDetailActivity, googleResult::class.java)
            val url = "https://www.google.com/search?q=${song.getSongName()}%20$artist%20lyrics"
            intent.putExtra("URL", url)
            startActivity(intent)
        }

        similar_list.layoutManager = GridLayoutManager(this, 2)

        viewModel = ViewModelProviders.of(this).get(SongViewModel::class.java)

        val observer = Observer<ArrayList<Song>> {
            similar_list.adapter = adapter
            val result = DiffUtil.calculateDiff(object : DiffUtil.Callback(){
                override fun areItemsTheSame(p0: Int, p1: Int): Boolean {
                    return SongList[p0].artist == SongList[p1].artist
                }

                override fun getOldListSize(): Int {
                    return SongList.size
                }

                override fun getNewListSize(): Int {
                    if (it == null) {
                        return 0
                    }

                    return it.size
                }

                override fun areContentsTheSame(p0: Int, p1: Int): Boolean {
                    return SongList[p0] == SongList[p1]
                }
            })
            result.dispatchUpdatesTo(adapter)
            SongList = it ?: ArrayList()
        }

        viewModel.getSimilarTracks(artist, song.getSongName()).observe(this, observer)
    }

    inner class SimilarAdapter: RecyclerView.Adapter<SimilarAdapter.SimilarViewHolder>() {
        override fun onCreateViewHolder(p0: ViewGroup, p1: Int): SimilarAdapter.SimilarViewHolder {
            val itemView = LayoutInflater.from(p0.context).inflate(R.layout.song_item, p0, false)
            return SimilarViewHolder(itemView)
        }

        override fun onBindViewHolder(p0: SimilarViewHolder, p1: Int) {
            val song = SongList[p1]

            p0.nameItem.text = song.getSongName()
            Picasso.with(this@SongDetailActivity).load(song.image[2]).into(p0.imageItem)

        }

        override fun getItemCount(): Int {
            return SongList.size
        }

        inner class SimilarViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            var imageItem: ImageView = itemView.songImage
            var nameItem: TextView = itemView.songName
        }
    }

    override fun onBackPressed() {

        this.finish()
    }
}
