package com.example.playlistapp

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import android.provider.BaseColumns

class PlayViewModel(application: Application): AndroidViewModel(application){
    // view model for the playlist fragment that gets data from the sql

    private var _playList: MutableLiveData<ArrayList<plSet>> = MutableLiveData()
    private var _playListDBHelper: PlayListDatabaseHelper = PlayListDatabaseHelper(application)

    fun getPlayList(): MutableLiveData<ArrayList<plSet>> {
        loadList()
        return _playList
    }

    private fun loadList() {
        val db = _playListDBHelper.readableDatabase

        val projection = arrayOf(BaseColumns._ID, DbSettings.DBPlayListEntry.COL_SONG, DbSettings.DBPlayListEntry.COL_ARTIST)

        val cursor = db.query(
            DbSettings.DBPlayListEntry.TABLE,
            projection,
            null,
            null,
            null,
            null,
            null
        )

        var songs = ArrayList<plSet>()
        with(cursor) {
            while (moveToNext()) {
                val name = getString(getColumnIndexOrThrow(DbSettings.DBPlayListEntry.COL_SONG))
                val artist = getString(getColumnIndexOrThrow(DbSettings.DBPlayListEntry.COL_ARTIST))
                val pl = plSet(name, artist)
                songs.add(pl)
            }
        }

        _playList.value = songs
    }

    // when delete button is pressed, receives context and song name from the playlist fragment
    // passes the song name to the deleteSong function in the database helper
    // updates the playlist
    public fun delete(context: Context, songName: String): MutableLiveData<ArrayList<plSet>>  {
        val db = PlayListDatabaseHelper(context)
        db.deleteSong(songName)
        return getPlayList()
    }
}