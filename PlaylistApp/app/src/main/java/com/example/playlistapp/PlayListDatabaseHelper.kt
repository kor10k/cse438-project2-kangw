package com.example.playlistapp

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.example.playlistapp.DbSettings.DBPlayListEntry.Companion.COL_SONG
import com.example.playlistapp.DbSettings.DBPlayListEntry.Companion.ID

class PlayListDatabaseHelper(context: Context): SQLiteOpenHelper(context, DbSettings.DB_NAME, null, DbSettings.DB_VERSION) {
    override fun onCreate(db: SQLiteDatabase?) {
        val createPlayListTableQuery = "CREATE TABLE " + DbSettings.DBPlayListEntry.TABLE + " ( " +
                DbSettings.DBPlayListEntry.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DbSettings.DBPlayListEntry.COL_SONG + " TEXT NULL, " +
                DbSettings.DBPlayListEntry.COL_ARTIST + " TEXT NULL);"

        db?.execSQL(createPlayListTableQuery)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL("DROP TABLE IF EXISTS " + DbSettings.DBPlayListEntry.TABLE)
        onCreate(db)
    }

    // adds song name and the artist strings to the playlist table passed from the songdetailactivity
    fun addSong(song: String, artist: String) {
        val db = this.writableDatabase

        val values = ContentValues().apply {
            put(DbSettings.DBPlayListEntry.COL_SONG, song)
            put(DbSettings.DBPlayListEntry.COL_ARTIST, artist)
        }
        val newRowId = db?.insert(DbSettings.DBPlayListEntry.TABLE, null, values)

    }

    // using the song name string passed from the songViewModel, finds the id with the matching song name and removes the row from the table
    fun deleteSong(song: String): Boolean {

        var result = false

        val query = "SELECT * FROM ${DbSettings.DBPlayListEntry.TABLE} WHERE ${DbSettings.DBPlayListEntry.COL_SONG} = \"$song\""

        val db = this.writableDatabase

        val cursor = db.rawQuery(query,
            null)

        if(cursor.moveToFirst()) {
            val id = Integer.parseInt(cursor.getString(0))
            db.delete(DbSettings.DBPlayListEntry.TABLE, DbSettings.DBPlayListEntry.ID + " = ?", arrayOf(id.toString()))
            cursor.close()
            result = true
        }
        db.close()
        return result
    }
}