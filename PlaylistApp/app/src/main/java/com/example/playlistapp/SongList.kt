package com.example.playlistapp

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.media.Image
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.util.DiffUtil
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.squareup.picasso.Picasso

import kotlinx.android.synthetic.main.fragment_play_list.*
import kotlinx.android.synthetic.main.fragment_song_list.*
import kotlinx.android.synthetic.main.song_item.*
import kotlinx.android.synthetic.main.song_item.view.*


@SuppressLint("ValidFragment")
class SongList(context: Context) : Fragment() {
    // This fragment file is for the home tab aka search tab aka top20 tab

    private var parentContext = context

    private var adapter = SongAdapter()

    private lateinit var viewModel: SongViewModel
    private var SongList: ArrayList<Song> = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_song_list, container, false)
    }

    override fun onStart() {
        super.onStart()

        // this portion is to check if a user typed in text when they press search
        // if the user did not enter any text, the program prompts the user to enter
        search_edit_text.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                val searchText = search_edit_text.text
                search_edit_text.setText("")
                if (searchText.toString() == "") {
                    val toast = Toast.makeText(this.parentContext, "Please enter text", Toast.LENGTH_SHORT)
                    toast.setGravity(Gravity.CENTER, 0, 0)
                    toast.show()
                    return@setOnEditorActionListener true
                }
                else {
                    performSearch(searchText.toString())
                    return@setOnEditorActionListener false
                }

            }
            return@setOnEditorActionListener false
        }



        songs_list.layoutManager = GridLayoutManager(this.context, 2)

        viewModel = ViewModelProviders.of(this).get(SongViewModel::class.java)


        val observer = Observer<ArrayList<Song>> {
            songs_list.adapter = adapter
            val result = DiffUtil.calculateDiff(object : DiffUtil.Callback(){
                override fun areItemsTheSame(p0: Int, p1: Int): Boolean {
                    return SongList[p0].artist == SongList[p1].artist
                }

                override fun getOldListSize(): Int {
                    return SongList.size
                }

                override fun getNewListSize(): Int {
                    if (it == null) {
                        return 0
                    }

                    return it.size
                }

                override fun areContentsTheSame(p0: Int, p1: Int): Boolean {
                    return SongList[p0] == SongList[p1]
                }
            })
            result.dispatchUpdatesTo(adapter)
            SongList = it ?: ArrayList()

        }
        // initially shows top 20 tracks on the home tab
        viewModel.getTopSongs().observe(this, observer)
    }

    // if a user searches for an artist, replaces viewModel to show the songs of the artist
    private fun performSearch(query: String) {

        viewModel.getArtistSongs(query)
    }

    // an adapter to set the item in the gridview
    inner class SongAdapter: RecyclerView.Adapter<SongAdapter.SongViewHolder>() {
        override fun onCreateViewHolder(p0: ViewGroup, p1: Int): SongViewHolder {
            val itemView = LayoutInflater.from(p0.context).inflate(R.layout.song_item, p0, false)
            return SongViewHolder(itemView)
        }

        override fun onBindViewHolder(p0: SongViewHolder, p1: Int) {
            //results taken from Last.fm
            val song = SongList[p1]
            val artist = song.artist.getArtistName()
            val playCount = song.getPlayCount()
            val listener = song.getListener()

            p0.nameItem.text = song.getSongName()
            Picasso.with(this@SongList.context).load(song.image[2]).into(p0.imageItem)

            // when the track is clicked, starts a SongDetailActivity
            p0.row.setOnClickListener {
                val intent = Intent(this@SongList.context, SongDetailActivity::class.java)
                intent.putExtra("SONG", song)
                intent.putExtra("ARTIST", artist)
                intent.putExtra("COUNT", playCount)
                intent.putExtra("LISTENER", listener)
                startActivity(intent)
            }
        }

        override fun getItemCount(): Int {
            return SongList.size
        }

        inner class SongViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
            val row = itemView
            var imageItem: ImageView = itemView.songImage
            var nameItem: TextView = itemView.songName
        }
    }


}
