package com.example.playlistapp

import android.annotation.SuppressLint
import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import android.os.AsyncTask
import android.util.Log
import com.example.playlistapp.cQueryUtils

class SongViewModel(application: Application): AndroidViewModel(application) {
    // view model that sets up query for 3 different api calls and returns the result from the api call
    private var _songList: MutableLiveData<ArrayList<Song>> = MutableLiveData()

    fun getTopSongs(): MutableLiveData<ArrayList<Song>> {
        loadSongs("?method=chart.gettoptracks&api_key=5c17ac401038f4476ee3da8e3a908dce&format=json")
        return _songList
    }

    private fun loadSongs(query: String) {
        SongAsyncTask().execute(query)
    }

    @SuppressLint("StaticFieldLeak")
    inner class SongAsyncTask: AsyncTask<String, Unit, ArrayList<Song>>() {
        override fun doInBackground(vararg params: String?): ArrayList<Song>? {
            return cQueryUtils.fetchSongData(params[0]!!)
        }

        override fun onPostExecute(result: ArrayList<Song>?) {
            var top20: ArrayList<Song> = ArrayList()
            if (result == null) {
                Log.e("RESULTS", "No Results Found")

            }
            else {

                Log.e("RESULTS", result.toString())

                for (i in 0 until 20){
                    top20.add(result[i])
                }
                _songList.value = top20
            }

        }
    }

    fun getArtistSongs(query: String) : MutableLiveData<ArrayList<Song>> {
        loadArtist("?method=artist.gettoptracks&artist=$query&api_key=5c17ac401038f4476ee3da8e3a908dce&format=json")
        return _songList
    }

    private fun loadArtist(query: String) {
        SearchAsyncTask().execute(query)
    }

    @SuppressLint("StaticFieldLeak")
    inner class SearchAsyncTask: AsyncTask<String, Unit, ArrayList<Song>>() {
        override fun doInBackground(vararg params: String?): ArrayList<Song>? {
            return cQueryUtils.fetchArtistSongData(params[0]!!)
        }

        override fun onPostExecute(result: ArrayList<Song>?) {
            var top20: ArrayList<Song> = ArrayList()
            if (result == null) {
                Log.e("RESULTS", "No Results Found")
            } else {

                Log.e("RESULTS", result.toString())

                for (i in 0 until 20){
                    top20.add(result[i])
                }
                _songList.value = top20
            }

        }
    }

    fun getSimilarTracks(artist: String, song: String) : MutableLiveData<ArrayList<Song>> {
        loadSimilar("?method=track.getsimilar&artist=$artist&track=$song&api_key=5c17ac401038f4476ee3da8e3a908dce&format=json")
        return _songList
    }

    private fun loadSimilar(query: String) {
        SimilarAsyncTask().execute(query)
    }

    @SuppressLint("StaticFieldLeak")
    inner class SimilarAsyncTask: AsyncTask<String, Unit, ArrayList<Song>>() {
        override fun doInBackground(vararg params: String?): ArrayList<Song>? {
            return cQueryUtils.fetchSimilarSongData(params[0]!!)
        }

        override fun onPostExecute(result: ArrayList<Song>?) {
            if (result == null) {
                Log.e("RESULTS", "No Results Found")

            } else {

                Log.e("RESULTS", result.toString())

                _songList.value = result
            }

        }
    }


}