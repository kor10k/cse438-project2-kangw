package com.example.playlistapp

import java.io.Serializable

class Artist(): Serializable {
    // class to store the artist info
    private var artistName: String = ""
    private var Mbid: String = ""
    private var artistUrl: String = ""

    constructor(
        artistName: String,
        Mbid: String,
        artistUrl: String
    ): this() {
        this.artistName = artistName
        this.Mbid = Mbid
        this.artistUrl = artistUrl
    }

    fun getArtistName(): String {
        return this.artistName
    }

    fun getMbid(): String {
        return this.Mbid
    }

    fun getArtistUrl(): String {
        return this.artistUrl
    }
}